import pymongo
import pandas as pd
import sys
import urllib.request
import zipfile
import json
import scipy
import pprint as pp
import numpy as np
import json
from scipy.stats import pearsonr

db = pymongo.MongoClient()["group2"]
collection = db["survey"]

def task_0():
    print("executing task 0")
    data_url = "https://island.ricerca.di.unimi.it/%7Ealfio/stack-overflow-2018-developer-survey.zip"
    file_name, headers = urllib.request.urlretrieve(data_url)
    zip_ref = zipfile.ZipFile(file_name, 'r')
    zip_ref.extractall(".")
    zip_ref.close()
    csv_results_filename = "survey_results_public.csv"
    df = pd.read_csv(csv_results_filename)
    collection.insert_many(df.to_dict("records"))

def task_1():
    print("executing task 1")
    match = {
        '$match': {
            'DevType': 'Database administrator' 
        }
    }

    group = {
        '$group': {
            '_id': '$Country' ,
            'answers': {
                '$sum': 1
            } 
        }
    }

    res = {}

    for x in collection.aggregate([match, group]):
        res[x['_id']] = x['answers']

    with open('task1.json', 'w') as file:
        file.write(json.dumps(res))
    
def task_2():
    print("executing task 2")
    match = {
	    "$match" : {"Age" : {"$ne" : np.nan}}
    }

    project = {		
            "$project": {
                "Country": 1,
                "Age" : 1,
                "age": {
                    "$cond": { 
                        "if": { "$or" : [ { "$eq": [ "$Age", "18 - 24 years old" ] }, { "$eq" : [ "$Age", "25 - 34 years old" ] }, { "$eq" : [ "$Age", "Under 18 years old" ] } ] }, 
                        "then": 0,
                        "else": 1
                    }
            }
        }
    }

    group = {
	    "$group" :{
			    "_id" : {"country" : "$Country", "age" : "$age"},
				"total" : {"$sum" : 1}
        }
    }

    group2 = {
		"$group" : {
			"_id" : {"country" : "$_id.country"},
			"list" : {"$push" : {"type" : "$_id.age", "count" : "$total"} }							
		}
    }


    pipeline = [match, project, group, group2]

    countries = []

    for record in collection.aggregate(pipeline):
        type0 = 0
        type1 = 0
        l = record["list"]
        for el in l:
            if(el["type"] == 0):
                type0 = el["count"]
            else:
                if(el["type"] == 1):
                    type1 = el["count"]

        if(type0 - type1 > 0):
            countries.append(record["_id"]["country"])

    d = dict()
    d["countries"] = countries

    with open("task2.json", "w") as file:
        file.write(json.dumps(d))

def task_3():
    print("executing task 3")
    drop_nan = {
        "$match" : {
            "LanguageWorkedWith" : {"$ne" : np.nan}
        }
    }
    project = { "$project" : 
            { "LanguageWorkedWith" : 
                { "$split": ["$LanguageWorkedWith", ";"] 
                }
            } 
    }
    unwind = {"$unwind" : "$LanguageWorkedWith"}
    group = {
        "$group" : {
            "_id" : "$LanguageWorkedWith",
            "dev_count" : {
                    "$sum" : 1
                }
            }
        }
    sort = {
        "$sort":{
            "dev_count" :pymongo.DESCENDING
        }
    }
    langs = list()
    for doc in collection.aggregate([drop_nan, project, unwind, group, sort]):
        langs.append(doc["_id"])
    answer = {'languages': langs}
    with open('task3.json', 'w') as fp:
        json.dump(answer, fp)

def task_4():
    print("excuting task 4")
    project = {
    '$project': {
        '_id': 0,
        'CompanySize': 1,
        'AssessJob4': 1
    }
    }

    match = {
    '$match': {
        'AssessJob4': { '$ne': np.nan },
        'CompanySize': { '$ne': np.nan }
        }
    }

    group = {
        '$group': {
            '_id': '$CompanySize',
            'avg_relevance': {
                '$avg': '$AssessJob4'
            }
        }
    }

    res = {}

    for x in collection.aggregate([project, match, group]):
        res[x['_id']] = x['avg_relevance']

    with open('task4.json', 'w') as file:
        file.write(json.dumps(res))

def task_5():
    print("excuting task 5")
    benefits = "AssessBenefits"
    benefits_list = []
    for i in range(1,12):
        benefits_list.append(benefits + str(i))

    drop_nan = {
        "$match" : {
            "AssessBenefits1" : {"$ne" : np.nan}
        }
    }
    results = dict()
    for benefit_one in benefits_list:
        results[benefit_one] = dict()
        for benefit_two in benefits_list:
            project = {
                "$project" :{
                    benefit_one : 1,
                    benefit_two : 1
                }
            }
            drop_nan = {
                "$match" : {
                    benefit_one : {"$ne" : np.nan},
                    benefit_two : {"$ne" : np.nan}
                }
            }
            list_one = []
            list_two = []
            for doc in collection.aggregate([project, drop_nan]):
                list_one.append(doc[benefit_one])
                list_two.append(doc[benefit_two])
            p = pearsonr(list_one, list_two)
            results[benefit_one][benefit_two] = p[0]
    with open('task5.json', 'w') as fp:
        json.dump(results, fp)

def task_6():
    pass
    
if __name__ == "__main__":
   task_number = sys.argv[1:][1]
   tasks = dict()
   tasks["0"] = task_0
   tasks["1"] = task_1
   tasks["2"] = task_2
   tasks["3"] = task_3
   tasks["4"] = task_4
   tasks["5"] = task_5
   tasks["6"] = task_6
   tasks[task_number]()