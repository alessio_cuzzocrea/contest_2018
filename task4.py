project = {
    '$project': {
        '_id': 0,
        'CompanySize': 1,
        'AssessJob4': 1
    }
}

match = {
    '$match': {
        'AssessJob4': { '$ne': np.nan },
        'CompanySize': { '$ne': np.nan }
    }
}

group = {
    '$group': {
        '_id': '$CompanySize',
        'avg_relevance': {
            '$avg': '$AssessJob4'
        }
    }
}

res = {}

for x in sur.aggregate([project, match, group]):
    res[x['_id']] = x['avg_relevance']

with open('task4.json', 'w') as file:
    file.write(json.dumps(res))