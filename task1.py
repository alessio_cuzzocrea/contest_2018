import pymongo
import pandas as pd
import numpy as np
import scipy
import pprint as pp
import json


// importazione csv
survey_csv = 'survey_results_public.csv'

survey = pd.read_csv(survey_csv, low_memory=False)



// creazione database e collection
db = pymongo.MongoClient()['group2']

db['survey'].insert_many(survey.to_dict('records'))

sur = db['survey']



// Task 1
match = {
    '$match': {
        'DevType': 'Database administrator' 
    }
}

group = {
    '$group': {
        '_id': '$Country' ,
        'answers': {
            '$sum': 1
        } 
    }
}

res = {}

for x in sur.aggregate([match, group]):
     res[x['_id']] = x['answers']

with open('task1.json', 'w') as file:
    file.write(json.dumps(res))